import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'nav-bar',
  styles: ['nav { height: 60px; background: #E2001A; border-radius: 10px; margin-bottom: 10px; } nav img  { width: auto; height: 100%; }'],
  templateUrl: 'nav.component.html'
})
export class NavbarComponent {
}
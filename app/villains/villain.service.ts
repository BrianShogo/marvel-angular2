import { Injectable } from '@angular/core';

import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class VillainService {
  constructor(private http: Http) { }

  getVillains() {
    return this.http.get('app/villains.json')
      .map((res: Response) => res.json());

    // return [
    //   { 'id': 1, 'name': '451' },
    //   { 'id': 2, 'name': 'Red Skull' },
    //   { 'id': 3, 'name': 'Venom' },
    //   { 'id': 4, 'name': 'Loki' },
    //   { 'id': 5, 'name': 'Madman' }
    // ];

  }
}

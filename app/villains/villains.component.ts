import { Component, OnInit } from '@angular/core';

import { Villain } from './villain.model';
import { VillainComponent } from './villain.component';
import { VillainService } from './villain.service';

@Component({
  moduleId: module.id,
  selector: 'toh-villains',
  templateUrl: 'villains.component.html',
  styleUrls: ['villains.component.css'],
  directives: [VillainComponent],
  providers: [VillainService]
})
export class VillainsComponent implements OnInit {
  villains: Villain[];
  selectedVillain: Villain;

  constructor(private villainService: VillainService) { }

  ngOnInit() {
      this.villainService.getVillains()
        .subscribe(villains => this.villains = villains);

    // this.villains = this.villainService.getVillains();

    // this.villains = [
    //   { 'id': 1, 'name': '451' },
    //   { 'id': 2, 'name': 'Red Skull' },
    //   { 'id': 3, 'name': 'Venom' },
    //   { 'id': 4, 'name': 'Loki' },
    //   { 'id': 5, 'name': 'Madman' }
    // ];
  }

  onSelect(villain: Villain){
    this.selectedVillain = villain;
  }

}



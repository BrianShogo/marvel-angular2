import { Component, Input } from '@angular/core';

import { Villain } from './villain.model';

@Component({
  moduleId: module.id,
  selector: 'toh-villain',
  templateUrl: 'villain.component.html'
})
export class VillainComponent {
  @Input() villain: Villain;
}

import {Component} from '@angular/core';

import { HeroesComponent } from './heroes/heroes.component';
import { NavbarComponent } from './shared/nav.component';

@Component({
    selector: 'my-app',
    template: `
    <nav-bar></nav-bar>
    <toh-heroes></toh-heroes>
    <toh-villains></toh-villains>
    `,
    directives: [HeroesComponent, NavbarComponent]
})
export class AppComponent {
}

import { Component, OnInit } from '@angular/core';

import { Hero } from './hero.model';
import { HeroComponent } from './hero.component';
import { HeroService } from './hero.service';

@Component({
  moduleId: module.id,
  selector: 'toh-heroes',
  templateUrl: 'heroes.component.html',
  styleUrls: ['heroes.component.css'],
  directives: [HeroComponent],
  providers: [HeroService]
})
export class HeroesComponent implements OnInit {
  heroes: Hero[];
  selectedHero: Hero;

  constructor(private heroService: HeroService) { }

  ngOnInit() {
      this.heroService.getHeroes()
        .subscribe(heroes => this.heroes = heroes);

    // this.heroes = this.heroService.getHeroes();

    // this.heroes = [
    //   { 'id': 1, 'name': 'Iron Man' },
    //   { 'id': 2, 'name': 'Captain America' },
    //   { 'id': 3, 'name': 'Spider-Man' },
    //   { 'id': 4, 'name': 'Thor' },
    //   { 'id': 5, 'name': 'Hulk' }
    // ];
  }

  onSelect(hero: Hero){
    this.selectedHero = hero;
  }

  removeSelected(){
    this.selectedHero = '';
  }
  
}



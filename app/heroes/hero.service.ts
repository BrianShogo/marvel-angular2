import { Injectable } from '@angular/core';

import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HeroService {
  constructor(private http: Http) { }

  getHeroes() {
    return this.http.get('app/heroes.json')
      .map((res: Response) => res.json());

    // return [
    //   { 'id': 1, 'name': 'Iron Man' },
    //   { 'id': 2, 'name': 'Captain America' },
    //   { 'id': 3, 'name': 'Spider-Man' },
    //   { 'id': 4, 'name': 'Thor' },
    //   { 'id': 5, 'name': 'Hulk' }
    // ];

  }
}

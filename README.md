# Angular 2 Demo

Clone it and run it!

```bash
npm install
npm start
```
Other links:

- [John's Angular 2 Pluralight Course](http://jpapa.me/a2ps1stlook) 
- [Angular 2 App in 17 Minutes Live Coding Demo](https://johnpapa.net/17-minute-angular-2-app/)
- [Ionic 2](http://ionic.io/2) -> Mobile
- [NativeScript](http://www.nativescript.org) ->Mobile
- [React Native](http://facebook.github.io/react-native/) -> Mobile
- [Electron](http://electron.atom.io/)
- [Angular Material](https://material.angular.io/)
- [Angular material demo](https://puppy-love.firebaseapp.com/)
- [TypeScript](http://typescriptlang.org)
- [Angular 2 Docs](https://angular.io/docs/ts/latest/)
- [Angular 2 Tutorial](https://angular.io/docs/ts/latest/tutorial/)
- [Angular 2 CLI](http://cli.angular.io)
- [Angular 2 Codelyzer](https://github.com/mgechev/codelyzer)
- [Code snippets in vs code](http://jpapa.me/a2vscode) 
- [Angular 2 Style Guide](http://jpapa.me/ng2styleguide) 
- [Angular 2 Workshop in Barcelona with Dan Wahlin](https://johnpapa.net/angular-2-workshop-in-barcelona/)
- [Angular 2 ngFor syntax change](https://johnpapa.net/angular-2-ngfor/)
- [Why TypeScript?](https://johnpapa.net/es5-es2015-typescript/)